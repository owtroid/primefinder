#!/usr/bin/env python
import sys
import array
import math

print "Looking for first ", sys.argv[1], " primes (skipping 2)"
maxPrimes = int(sys.argv[1])

primes = array.array('i')
#primes.append();

candidate = 3
while len(primes) < maxPrimes-1:
    candidateIsPrime = 1
    for prime in primes:
        if (prime > math.sqrt(candidate)):
            break
        if (candidate % prime) == 0:
            candidateIsPrime = 0
            break
    if candidateIsPrime:
        primes.append(candidate)
        print "\r", len(primes)+1, " primes found",
    candidate += 2

#primes.insert(0,2)
print ""
print "Found", len(primes)+1, "primes"
print "The last primes are:"
count = len(primes) - 10
if count < 0:
    count = 0

while count < len(primes):
    print primes[count]
    count += 1
