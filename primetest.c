#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static const char filename[] = "file.txt";
FILE *file;

void open_file(char *mode) {
  file = fopen(filename, mode);
}
void close_file() {
  fflush(file);
  fclose(file);
}

void append_prime(unsigned long long prime) {
  int res;
  open_file("a");
  
  res = fprintf(file, "%lld\n", prime);
  if (res < 0) {
    perror("Failed to append prime");
  }

  close_file();
}

unsigned long long read_next_prime() {
  if (file != NULL) {
    char line [ 128 ]; /* or other suitable maximum line size */
    if (fgets(line, sizeof line, file) != NULL) {
      return atoll(line);
    }
  } else {
    perror(filename); /* why didn't the file open? */
  }
  return 0;
}

unsigned long long read_last_prime() {
  long size;
  char line [128];
  unsigned long long prime;
  int res;
  open_file("r");
  res = fseek(file, 0, SEEK_END);
  if (res<0) {
    perror("read_last_prime");
  }
  size = ftell(file);
  if (size > 1024)
    size = size - 1024; /* go back 1024 bytes should be enough */
  else
    size = 0;
  res = fseek(file, size, SEEK_SET);
  if (res<0) {
    perror("read_last_prime");
  }
  while (fgets(line, sizeof line, file) != NULL) {
      prime = atoll(line);
  }
  close_file(file);
  return prime;
}

void find_primes(unsigned long long max_primes) {
  unsigned long long candidate;
  unsigned long long prime_count = 1;

  candidate = read_last_prime();
  if (candidate < 3) {
    candidate = 3;
  }
  
  printf("Starting search from %lld\n", candidate);
  while (prime_count < max_primes) {
    int candidate_is_prime = 1;
    unsigned long long prime;
    open_file("r");
    while ((prime = read_next_prime()) != 0) {
      if (prime > sqrt(candidate)) {
	break;
      }
      if ((candidate % prime) == 0) {
	candidate_is_prime = 0;
	break;
      }
    }
    close_file();
    if (candidate_is_prime) {
      prime_count++;
      append_prime(candidate);
      //printf("\r %lld primes found", prime_count);
    }
    candidate += 2;
  }

  printf("\n");
  printf("Found %lld primes\n", prime_count);
}

int main(int argc, char *argv[]) {
  if (argc < 0) {
    printf("Insufficient arguments\n");
    return 1;
  }

  read_last_prime();

  printf("Converting input\n");
  unsigned long long max_primes = (unsigned long long) atoll(argv[1]);
  printf("Starting algorithm\n");
  find_primes(max_primes);

  return 0;
}
